import React, { Component } from "react";
import  { useState } from "react";
import { v4 } from "uuid";
import todosList from "./todos.json";


function App () {
  const [todos,setTodos] = useState(todosList)
  const [input,setInput] = useState("")
  function addItem (event) {
    if(event.key === "Enter"){
      event.preventDefault()
      const newItem = {
      userId: 1,
      id: v4,
      title: `${event.target.value}`,
      completed: false
    }
    setTodos([...todos, newItem])
    console.log(newItem)
    event.target.value = ""
    
  }
}
  function TodoList ({todos,toggleItem,subtractItem}) {
    return (
      <section className="main">
        <ul className="todo-list">
          {todos.map((todo) => (
            <TodoItem
            key={todo.title}
            toggleItem = {toggleItem}
            subtractItem ={subtractItem}
            todo={todo} />
          ))}
        </ul>
      </section>
    );
}
function TodoItem ({todo, toggleItem, subtractItem}) {
      const {
        completed,
        id,
        title
      } = todo
  return (
    <li className={completed ? "completed" : ""}>
      <div className="view">
        <input className="toggle"
         type="checkbox"
         checked={completed}
         onClick = {() => toggleItem(id)}/>
        <label>{title}</label>
        <button className="destroy" onClick = {() => subtractItem(id)} />
      </div>
    </li>
  );
}
function toggleItem (id) {
  const newToDos = todos.map((todo)=> {
    if(id === todo.id){
      return {...todo, completed: !todo.completed}
      }
    return todo
  })
    setTodos(newToDos)
}
function subtractItem (id) {
  const newToDos = todos.filter((todo)=> {
   const keptToDos = id !== todo.id
   return keptToDos
  })
    setTodos(newToDos)
}
function removeCompleted () {
  const newToDos = todos.filter((todo) => {
    const keptToDos =  !todo.completed
    return keptToDos
  })
    setTodos(newToDos)
}
  
    return (
      <section className="todoapp">
        <header className="header">
          <h1>To dos</h1>
          <input
          className="new-todo"
          placeholder="What needs to be done?"
          autoFocus
           onChange = {(event) => setInput(event.target.value)}
           onKeyDown = {addItem}
           />
        </header>
        <TodoList todos={todos}
        toggleItem = {toggleItem}
        subtractItem = {subtractItem}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button
          className="clear-completed"
          onClick = {() => removeCompleted()}
          >Clear completed</button>
        </footer>
      </section>
    );
}
export default App;


// class App extends Component {
//   state = {
//     todos: todosList,
//   };
//   handleAddTodo = (evt) => {
//     if (evt.key === "Enter"){
//       const newTodo = {
//         "userId": 1,
//         "id": v4(),
//         "title": evt.target.value,
//         "completed": false
      
//     };
//     const newTodos = this.state.todos.slice()
//       newTodos.push(newTodo);
//     this.setState({ todos: newTodos });
//     evt.target.value=""
//   }
//   };

//   handleComplete = (id) =>{
//     let newTodos = this.state.todos.map((todo)=> {
//       if(todo.id === id) {
//         return{...todo, completed: !todo.completed};
//       }
//         return todo;
//     });
//     this.setState({todos: newTodos});
//   };

//   handleDelete =(todoItemId)=>{
//     const newTodos = this.state.todos.filter((todo)=> todo.id !==todoItemId);
//     console.log("delete");
//     this.setState({todos:newTodos});
//   };
//   handleClearComplete = () =>{
//     let clearedCompleteTodos = this.state.todos.filter(
//       (todo) =>todo.completed === false
//     );
//     console.log("clear");
//     this.setState({todos:clearedCompleteTodos});
//   };

//   render() {
//     return (
//       <section className="todoapp">
//         <header className="header">
//           <h1>To dos</h1>
//           <input className="new-todo" placeholder="What needs to be done?" autofocus />
//         </header>
//         <TodoList todos={this.state.todos} />
//         <footer className="footer">
//           <span className="todo-count">
//             <strong>0</strong> item(s) left
//           </span>
//           <button className="clear-completed">Clear completed</button>
//         </footer>
//       </section>
//     );
//   }
// }

// class TodoItem extends Component {
//   render() {
//     return (
//       <li className={this.props.completed ? "completed" : ""}>
//         <div className="view">
//           <input className="toggle" type="checkbox" checked={this.props.completed} />
//           <label>{this.props.title}</label>
//           <button className="destroy" />
//         </div>
//       </li>
//     );
//   }
// }

// class TodoList extends Component {
//   render() {
//     return (
//       <section className="main">
//         <ul className="todo-list">
//           {this.props.todos.map((todo) => (
//             <TodoItem title={todo.title} completed={todo.completed} />
//           ))}
//         </ul>
//       </section>
//     );
//   }
// }

// export default App;

